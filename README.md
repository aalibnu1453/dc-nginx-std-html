Command :
<br>&ensp;build and run container : $ docker-compose up -d
<br>&ensp;check list container : $ docker ps --format "table{{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Ports}}"
<br>
<br>
check curl in terminal :
<br>&emsp;$ curl -I http://[your local ip]:8998
<br>
<br>
testing in browser :
<br>&emsp;address http://[your local ip]:8998
